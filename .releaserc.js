module.exports = {
        "branches": [
          "main"
        ],
        "plugins": [
          ["@semantic-release/commit-analyzer", {
            "releaseRules": [
              {"type": "fix", "release": "patch"},
              {"type": "feat", "release": "minor"},
              {"type": "perf", "release":"major"},
              {"type": "ci", "release": "patch"},
              {"type": "chore", "release": "patch"}
            ]
          }
        ],
          "@semantic-release/release-notes-generator",
          "@semantic-release/gitlab",
          "@semantic-release/npm",
          [
            "@semantic-release/git",
            {
              "assets": [
                "package.json"
              ],
              "message": "chore(release): ${nextRelease.version}\n\n${nextRelease.notes}"
            }
          ]
        ],
        "publishConfig": {
          "access": "public"
        }
} 